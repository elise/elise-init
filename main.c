#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>

int main() {
	struct dirent *de;
	
	DIR *dir = opendir("/home/elise/.elise-init");
	
	char nameBuf[400] = {0};
	strcpy(nameBuf, "/bin/sh /home/elise/.elise-init/");
	while((de = readdir(dir)) != NULL) {
		if (de->d_name[0] != '.'){
			memcpy(nameBuf + strlen("/bin/sh /home/elise/.elise-init/"), de->d_name, strlen(de->d_name) + 1);
			system(nameBuf);
		}
		
	}
	return 0;
}
